const FormType = {
    select: 'select',
    radio: 'radio',
    checkbox: 'checkbox',
    text: 'text'
}

const groesse = {
    type: FormType.radio,
    label: 'Größe',
    values: {}
};
const fleisch = {
    type: FormType.checkbox,
    label: 'Fleisch',
    values: {
        haehnchen: {
            label: 'Hähnchen',
            preis: 0
        },
        kalb: {
            label: 'Kalb',
            preis: 0,
            default: true
        }
    },
    nothing: 'OHNE Fleisch',
    all: 'Beide Fleischsorten'
};
const sauce = {
    type: FormType.checkbox,
    label: 'Sauce',
    values: {
        joghurt: {
            label: 'Joghurt',
            preis: 0,
            default: true
        },
        knobi: {
            label: 'Knoblauch',
            preis: 0
        }
    },
    nothing: 'OHNE Sauce',
    all: 'Beide Saucen'
};
const ohne = {
    type: FormType.checkbox,
    label: 'Ohne',
    values: {
        zwiebeln: {
            label: 'Zwiebeln',
            preis: 0
        },
        salat: {
            label: 'Salat',
            preis: 0
        }
    },
    nothing: 'Mit Allem'
};
const extra = {
    type: FormType.checkbox,
    label: 'Extra',
    values: {
        kaese: {
            label: 'Käse',
            preis: 1
        },
        fleisch: {
            label: 'Fleisch',
            preis: 1.5
        }
    }
};
const scharf = {
    type: FormType.radio,
    label: 'Scharf',
    values: {
        nein: {
            label: 'Nein',
            preis: 0,
            default: true
        },
        etwas: {
            label: 'Etwas',
            preis: 0
        },
        extra: {
            label: 'Extra',
            preis: 0
        }
    }
};
const pommes = {
    type: FormType.checkbox,
    label: 'Für die Pommes',
    values: {
        ketchup: {
            label: 'Ketchup',
            preis: 0
        },
        mayo: {
            label: 'Mayo',
            preis: 0,
            default: true
        }
    },
    nothing: 'Pommes OHNE Sauce'
};
const getraenk = {
    type: FormType.radio,
    label: 'Getränk',
    values: {
        cola: {
            label: 'Cola',
            preis: 0,
            default: true
        },
        fanta: {
            label: 'Fanta',
            preis: 0
        },
        sprite: {
            label: 'Sprite',
            preis: 0
        },
        uludag: {
            label: 'Uludag',
            preis: 0
        },
        ayran: {
            label: 'Ayran',
            preis: 0
        }
    }
};

const karte = {
    doener: {
        label: 'Döner',
        preis: 7,
        groesse: {
            klein: {
                label: 'Klein',
                preis: -1.5
            },
            normal: {
                label: 'Normal',
                preis: 0,
                default: true
            }
        },
        extra: {
            kaese: {
                label: 'Käse',
                preis: 1.5
            }
        },
        configure: {
            groesse,
            fleisch,
            sauce,
            ohne,
            extra,
            scharf
        }
    },
    doener_xxl: {
        label: 'XXL Döner',
        preis: 9,
        extra: {
            fleisch: {
                label: 'Fleisch',
                preis: 2
            }
        },
        configure: {
            fleisch,
            sauce,
            ohne,
            extra,
            scharf
        }
    },
    doener_fritz: {
        label: 'Döner Fritz',
        preis: 7,
        groesse: {
            klein: {
                label: 'Klein',
                preis: 0,
                default: true
            },
            gross: {
                label: 'Groß',
                preis: 2
            }
        },
        extra: {
            salat: {
                label: 'Salat',
                preis: 0.5
            },
            zwiebeln: {
                label: 'Zwiebeln',
                preis: 0.5
            },
            beide: {
                label: 'Salat + Zwiebeln',
                preis: 0.5
            }
        },
        ohne: {
            pommes: {
                label: "Pommes",
                preis: 0
            }
        },
        configure: {
            groesse,
            fleisch,
            sauce,
            ohne,
            extra,
            scharf
        }
    },
    doener_menu: {
        label: 'Döner Menü',
        preis: 11.5,
        groesse: {
            mini: {
                label: 'Mini',
                preis: -2
            },
            maxi: {
                label: 'Maxi',
                preis: 0,
                default: true
            }
        },
        configure: {
            groesse,
            fleisch,
            sauce,
            ohne,
            extra,
            scharf,
            pommes,
            getraenk
        }
    },
    doener_teller: {
        label: 'Döner Teller',
        preis: 12,
        extra: {
            salat: {
                label: 'Salat',
                preis: 0.5
            }
        },
        configure: {
            fleisch,
            sauce,
            ohne,
            extra,
            scharf,
            pommes
        }
    },
    fladenbrot: {
        label: 'Veg. Fladenbrot',
        preis: 5.5,
        groesse: {
            klein: {
                label: 'Klein',
                preis: -1
            },
            gross: {
                label: 'Groß',
                preis: 0,
                default: true
            }
        },
        extra: {
            fleisch: {
                unset: true
            }
        },
        configure: {
            groesse,
            sauce,
            ohne,
            extra,
            scharf
        }
    },
    lahmacun: {
        label: 'Lahmacun',
        preis: 8,
        configure: {
            fleisch,
            sauce,
            ohne,
            extra,
            scharf
        }
    },
    dueruem: {
        label: 'Dürüm',
        preis: 7,
        configure: {
            fleisch,
            sauce,
            ohne,
            extra,
            scharf
        }
    },
    lahmacun_menu: {
        label: 'Maxi Lahmacun Menü',
        preis: 12.5,
        configure: {
            fleisch,
            sauce,
            ohne,
            extra,
            scharf,
            pommes,
            getraenk
        }
    },
    feta: {
        label: 'Feta Spezial',
        preis: 6.5,
        configure: {
            sauce,
            ohne,
            extra,
            scharf
        }
    },
    feta_teller: {
        label: 'Feta Teller',
        preis: 10.5,
        configure: {
            sauce,
            ohne,
            extra,
            scharf,
            pommes
        }
    },
    feta_menu: {
        label: 'Feta Menü',
        preis: 11,
        configure: {
            sauce,
            ohne,
            extra,
            scharf,
            pommes,
            getraenk
        }
    },
    falafel: {
        label: 'Falafel mit Brot',
        preis: 6.50,
        extra: {
            dueruem: {
                label: "Dürüm statt Brot",
                preis: 0
            }
        },
        configure: {
            sauce,
            ohne,
            extra,
            scharf
        }
    },
    falafel_teller: {
        label: 'Falafel Teller',
        preis: 10.5,
        configure: {
            sauce,
            ohne,
            scharf,
            pommes
        }
    },
    falafel_menu: {
        label: 'Falafel Menü',
        preis: 11,
        configure: {
            sauce,
            ohne,
            scharf,
            pommes,
            getraenk
        }
    },
    salat: {
        label: 'Salatteller',
        preis: 5.5,
        extra: {
            fleisch: {
                unset: true
            }
        },
        configure: {
            sauce,
            ohne,
            extra,
            scharf
        }
    },
    salat_haehnchen: {
        label: 'Hähnchensalat',
        preis: 7.5,
        configure: {
            sauce,
            ohne,
            extra,
            scharf
        }
    },
    boerek: {
        label: 'Sigara Börek',
        preis: 5,
        groesse: {
            einer: {
                label: '1 Stück',
                preis: -4,
            },
            sechser: {
                label: '6 Stück',
                preis: 0,
                default: true
            }
        },
        configure: {
            groesse
        }
    },
    nuggets: {
        label: 'Chicken Nuggets',
        preis: 4.5,
        groesse: {
            sechser: {
                label: '6 Stück',
                preis: 0,
                default: true
            },
            neuner: {
                label: '9 Stück',
                preis: 1,
            }
        },
        extra: {
            kaese: {
                unset: true
            },
            fleisch: {
                unset: true
            },
            pommes: {
                label: 'Pommes',
                preis: 2
            },
            ketchup: {
                label: 'Ketchup',
                preis: 0.2
            },
            mayo: {
                label: 'Majo',
                preis: 0.2
            },
            joghurt: {
                label: 'Joghurtsauce',
                preis: 0.2
            },
            knoblauch: {
                label: 'Knoblauchsauce',
                preis: 0.2
            }
        },
        configure: {
            groesse,
            extra
        }
    },
    pommes: {
        label: 'Pommes',
        preis: 3.5,
        groesse: {
            klein: {
                label: 'klein',
                preis: -0.5
            },
            gross: {
                label: 'Groß',
                preis: 0,
                default: true
            },
            doppel: {
                label: 'Doppel',
                preis: 1.5
            }
        },
        extra: {
            kaese: {
                unset: true
            },
            fleisch: {
                unset: true
            },
            ketchup: {
                label: 'Ketchup',
                preis: 0.2
            },
            mayo: {
                label: 'Majo',
                preis: 0.2
            },
            joghurt: {
                label: 'Joghurtsauce',
                preis: 0.2
            },
            knoblauch: {
                label: 'Knoblauchsauce',
                preis: 0.2
            }
        },
        configure: {
            groesse,
            extra
        }
    }
};

/* ************************************* */
/* ************************************* */
/* ************************************* */

const elem_bestellung = document.getElementById('bestellung');
const elem_auswahl = document.getElementById('auswahl');
const elem_details = document.getElementById('details');
const elem_preis = document.getElementById('preis');
const elem_text = document.getElementById('bestellung-text');
var v_auswahl;
var v_merged_details;

function toEurString(val) {
    return (val.toFixed(2) + " €").replace('-', '- ');
}

function makeLine(line) {
    return line.trim() ? '  - ' + line.trim() + '\n' : '';
}

function makeOrder() {
    var v_preis = v_auswahl.preis;
    var v_text = '';

    for (var [k_detail, v_detail] of Object.entries(v_merged_details)) {
        var options_count = 0;
        var checked_labels = [];

        for (var [k_option, v_option] of Object.entries(v_detail.values)) {
            if (v_option.unset) {
                continue;
            }
            if (document.getElementById(`${k_detail}-${k_option}`).checked) {
                v_preis += v_option.preis;
                checked_labels.push(v_option.label);
            }
            options_count++;
        }

        if (checked_labels.length == 0 && v_detail.nothing) {
            v_text += makeLine(v_detail.nothing);
        } else if (checked_labels.length == options_count && v_detail.all) {
            v_text += makeLine(v_detail.all);
        } else if (checked_labels.length > 0) {
            v_text += makeLine(`${v_detail.label}: ${checked_labels.join(' + ')}`);
        }
    }

    for (var line of document.getElementById('anmerkung').value.split('\n')) {
        v_text += makeLine(line);
    }

    var v_text = `${v_auswahl.label} // ${toEurString(v_preis)}\n` + v_text;

    elem_preis.innerText = toEurString(v_preis);
    elem_text.innerText = v_text;
}

function makeClickable(k_detail, v_detail, type) {
    var container_div = document.createElement('div');
    var detail_h3 = document.createElement('h3');
    
    container_div.classList.add('container-div');
    detail_h3.innerText = v_detail.label;
    container_div.appendChild(detail_h3);

    for (var [k_prop, v_prop] of Object.entries(v_detail.values)) {
        if (v_prop.unset) {
            continue;
        }

        var inner_div = document.createElement('div');
        var input = document.createElement('input');
        var label = document.createElement('label');

        input.type = type;
        input.name = k_detail;
        input.value = k_prop;
        input.id = `${k_detail}-${k_prop}`;
        input.checked = Boolean(v_prop.default);
        input.addEventListener('change', makeOrder);

        label.htmlFor = `${k_detail}-${k_prop}`;
        label.innerHTML = v_prop.label;

        if (v_prop.preis != 0) {
            label.innerHTML += ` (${toEurString(v_prop.preis)})`;
        }

        inner_div.classList.add('inner-div');
        inner_div.appendChild(input);
        inner_div.appendChild(label);

        container_div.appendChild(inner_div);
    }

    return container_div;
}

function makeRadio(k_detail, v_detail) {
    return makeClickable(k_detail, v_detail, 'radio');
}

function makeCheckbox(k_detail, v_detail) {
    return makeClickable(k_detail, v_detail, 'checkbox');
}

function makeInput(k_detail, v_detail) {
    switch (v_detail.type) {
        case FormType.radio:
            return makeRadio(k_detail, v_detail);

        case FormType.checkbox:
            return makeCheckbox(k_detail, v_detail);

        default:
            var span = document.createElement('span');
            span.innerHTML = `${k_detail} type unknown.`;
            return span;
    }
}

function makeAnmerkung() {
    var container_div = document.createElement('div');
    container_div.classList.add('container-div');

    var anmerkung_h3 = document.createElement('h3');
    anmerkung_h3.innerText = 'Sonstige Anmerkung'
    container_div.appendChild(anmerkung_h3);

    var inner_div = document.createElement('div');
    var anmerkung = document.createElement('textarea');
    anmerkung.id = "anmerkung";
    inner_div.appendChild(anmerkung);
    container_div.appendChild(inner_div);
    elem_details.appendChild(container_div);

    anmerkung.addEventListener('keyup', makeOrder);
    anmerkung.addEventListener('change', makeOrder);
}

function makeForm(k_auswahl) {
    v_auswahl = karte[k_auswahl];
    v_merged_details = {};

    while (elem_details.hasChildNodes()) {
        elem_details.removeChild(elem_details.firstChild);
    }

    for (var [k_detail, v_detail] of Object.entries(v_auswahl.configure)) {
        var v_merged_detail = structuredClone(v_detail);
        v_merged_detail.values = { ...v_detail.values, ...v_auswahl[k_detail] };
        v_merged_details[k_detail] = v_merged_detail;
        elem_details.appendChild(makeInput(k_detail, v_merged_detail));
    }

    makeAnmerkung();
    makeOrder();
}

for (var [k_auswahl, v_auswahl] of Object.entries(karte)) {
    var option = document.createElement('option');
    option.value = k_auswahl;
    option.innerText = `${v_auswahl.label} (${toEurString(v_auswahl.preis)})`;
    elem_auswahl.appendChild(option);
}

elem_bestellung.addEventListener('submit', (e) => {
    e.preventDefault();
    e.stopPropagation();

    navigator.clipboard.writeText('```\n' + elem_text.innerText + '```\n')
        .then(() => alert('Die Bestellung wurde in die Zwischenablage kopiert!\nFüge sie nun im Chat ein.'))
        .catch(() => alert('Hoppla, die Bestellung konnte nicht kopiert werden!\nKopiere sie bitte manuell und füge sie im Chat ein.'));


    return false;
});

elem_auswahl.addEventListener('change', (e) => {
    makeForm(e.target.value);
});
